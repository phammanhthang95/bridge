package pkg

type Computer interface {
	print()
	setPrinter(Printer)
}