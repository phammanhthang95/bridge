package pkg

import "fmt"

type Mac struct {
	printer Printer
}

func (w *Mac) Print() {
	fmt.Println("Print request for mac")
	w.printer.PrintFile()
}

func (w *Mac) SetPrinter(printer Printer) {
	w.printer = printer
}