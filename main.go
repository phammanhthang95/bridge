package main

import "bridge/pkg"

func main() {
	acerPrinter := &pkg.Acer{}
	asusPrinter := &pkg.Asus{}

	macComputer := &pkg.Mac{}
	macComputer.SetPrinter(acerPrinter)
	macComputer.Print()

	macComputer.SetPrinter(asusPrinter)
	macComputer.Print()

	winComputer := &pkg.Windows{}
	winComputer.SetPrinter(acerPrinter)
	winComputer.Print()

	winComputer.SetPrinter(asusPrinter)
	winComputer.Print()
}